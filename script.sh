#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

cd "${SCRIPT_DIR}"

jsonnet --string aws-nuke.jsonnet > aws-nuke.yaml

ulimit -n 65536 200000

aws-nuke \
  --max-wait-retries 10 \
  -q -c aws-nuke.yaml \
  --access-key-id "${AWS_ACCESS_KEY_ID}" \
  --secret-access-key "${AWS_SECRET_ACCESS_KEY}" \
  "$@"

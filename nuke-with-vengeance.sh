#!/usr/bin/env bash

set -euo pipefail
IFS=$'\n\t'


function delete_secret_replicas() {
  local region="$1"
  while read -r secret_id; do 
    while read -r replica_region; do
      aws --region "$region" secretsmanager remove-regions-from-replication --secret-id "$secret_id" --remove-replica-regions "$replica_region"
    done < <(aws --region "$region" secretsmanager describe-secret --secret-id "$secret_id"|jq -r '.ReplicationStatus[]|.Region')
  done < <(aws --region "$region" secretsmanager list-secrets --filters "Key=primary-region,Values=$region"|jq -r '.SecretList[]|.Name'|grep gitlab/dedicated/tenants/)
}

function delete_rds_instances() {
  local region="$1"
  while read -r db_instance; do 
    aws --region "$region" rds delete-db-instance --db-instance-identifier "$db_instance" --skip-final-snapshot
  done < <(aws --region "$region" rds describe-db-instances | jq -r '.DBInstances[]|select(.DBInstanceStatus != "deleting")|.DBInstanceIdentifier'|grep itest)
}

function prepare_region_for_nuke() {
  local region="$1"
  delete_secret_replicas "$region"
  delete_rds_instances "$region"
}

prepare_region_for_nuke eu-west-1
prepare_region_for_nuke eu-west-2
prepare_region_for_nuke us-east-1

echo nuke | cloud-nuke aws --config cloudnuke.yaml --resource-type s3 --resource-type cloudwatch-loggroup

(while true; do echo gldt-sandbox-andrew-c42c5285; sleep 10; done)|./script.sh --no-dry-run 

local removeByInstrumentorProjectTag(resourceTypes) =
  std.foldl(
    function(memo, r)
      memo {
        [r]: [
          {
            property: 'tag:Project',
            value: 'instrumentor',
            invert: true,
          },
        ],
      },
    resourceTypes,
    {},
  );

local config =
  {
    regions: [
      'eu-west-1',
      'eu-west-2',
      'us-east-1',
      // 'eu-west-2',
      // 'us-east-1',
      // 'us-east-2',
      // 'us-west-1',
      // 'us-west-2',
      // 'eu-central-1',
      // 'eu-north-1',
      // 'eu-south-1',
      // 'eu-west-2',
      // 'ca-central-1',
    ],
    'account-blocklist': [
      111111111111,
    ],
    accounts: {
      '452919332838': {
        presets: [
          'itest',
        ],
      },
    },
    'feature-flags': {
      'disable-deletion-protection': {
        RDSInstance: true,
        EC2Instance: true,
        CloudformationStack: true,
      },
    },
    'resource-types': {
      excludes: [
        'CognitoIdentityPool',
        'CognitoIdentityProvider',
        'CognitoUserPool',
        'CognitoUserPoolClient',
        'CognitoUserPoolDomain',
        'FMSNotificationChannel',
        'FMSPolicy',
        'IAMUser',
        'RoboMakerDeploymentJob',
        'RoboMakerFleet',
        'RoboMakerRobot',
        'RoboMakerRobotApplication',
        'RoboMakerSimulationApplication',
        'RoboMakerSimulationJob',
        'MediaConvertJobTemplate',
        'MediaConvertPreset',
        'MediaConvertQueue',
        'MediaLiveChannel',
        'MediaLiveInput',
        'MediaLiveInputSecurityGroup',
        'MediaPackageChannel',
        'MediaPackageOriginEndpoint',
        'MediaStoreContainer',
        'MediaStoreDataItems',
        'MediaTailorConfiguration',
        'KinesisVideoProject',
        'AWS::Timestream::Table',
        'AWS::AppRunner::Service',
        'MachineLearningBranchPrediction',
        'MachineLearningDataSource',
        'MachineLearningEvaluation',
        'MachineLearningMLModel',
        'AWS::Timestream::ScheduledQuery',
        'AWS::Timestream::Database',
        'AWS::MWAA::Environment',
        'WorkLinkFleet',
        'MigrationServiceCertificate',
        'MigrationServiceEndpoint',
        'MigrationServiceEventSubscription',
        'MigrationServiceReplicationInstance',
        'MigrationServiceReplicationTask',
        'MigrationServiceSubnetGroup',
        'GlueClassifier',
        'GlueConnection',
        'GlueCrawler',
        'GlueDatabase',
        'GlueDevEndpoint',
        'GlueJob',
        'GlueTrigger',
        'APIGatewayAPIKey',
        'APIGatewayClientCertificate',
        'APIGatewayDomainName',
        'APIGatewayRestAPI',
        'APIGatewayUsagePlan',
        'APIGatewayV2API',
        'APIGatewayV2VpcLink',
        'APIGatewayVpcLink',
        'CloudTrailTrail',

        'EC2Snapshot' // Delete these as AWS Backup Recovery Points
      ],
    },
    presets: {
      itest: {
        filters:
          removeByInstrumentorProjectTag([
            'RDSClusterSnapshot',
            'RDSDBCluster',
            'RDSDBClusterParameterGroup',
            'RDSDBParameterGroup',
            'RDSDBSubnetGroup',
            'RDSEventSubscription',
            'RDSInstance',
            'RDSOptionGroup',
            'RDSProxy',
            'RDSSnapshot',
            'CloudTrailTrail',
            'SecretsManagerSecret',


            'ACMCertificate',
            'EC2Snapshot',
            'EC2VPC',
            'EC2Volume',
          ])
          +
          {
            S3Bucket: [
              {
                property: 'Name',
                type: 'glob',
                value: 'itest*',
                invert: true,
              },
            ],
            S3Object: [
              {
                property: 'Bucket',
                type: 'glob',
                value: '*itest*',
                invert: true,
              },
            ],
            EC2Subnet: [
              {
                property: 'tag:Name',
                type: 'glob',
                value: 'itest*',
                invert: true,
              },
            ],
            EC2InternetGateway: [
              {
                property: 'tag:Name',
                type: 'glob',
                value: 'itest*',
                invert: true,
              },
            ],
            EC2InternetGatewayAttachment: [
              {
                property: 'tag:vpc:Name',
                type: 'glob',
                value: 'itest*',
                invert: true,
              },
            ],
            EC2Instance: [
              {
                property: 'tag:gitlab_node_prefix',
                type: 'glob',
                value: 'itest*',
                invert: true,
              },
            ],
            EC2RouteTable: [
              {
                property: 'tag:EC2Subnet',
                type: 'glob',
                value: 'itest*',
                invert: true,
              },
            ],
            EC2SecurityGroup: [
              {
                property: 'Name',
                type: 'glob',
                value: 'itest*',
                invert: true,
              },
            ],
            EC2Address: [
              {
                property: 'tag:Name',
                type: 'glob',
                value: 'itest*',
                invert: true,
              },
            ],
            EKSCluster: [
              {
                type: 'glob',
                value: 'itest*',
                invert: true,
              },
            ],
            EKSNodegroups: [
              {
                property: 'Cluster',
                type: 'glob',
                value: 'itest*',
                invert: true,
              },
            ],
            EC2KeyPair: [
              {
                type: 'glob',
                value: '*itest*',
                invert: true,
              },
            ],

            KMSAlias: [
              {
                property: 'Name',
                type: 'glob',
                value: 'alias/itest*',
                invert: true,
              },
            ],
            DynamoDBTable: [
              {
                property: 'tag:Project',
                value: 'amp',
              },
            ],
            KMSKey: [
              {
                property: 'tag:Project',
                value: 'amp',
              },
            ],
            AWSBackupRecoveryPoint: [],

            // Delete all backup selections
            AWSBackupSelection: [],
            AWSBackupPlan: [],
            AWSBackupVault: [],
            AWSBackupVaultAccessPolicy: [],
          },
      },
    },
  };

std.manifestYamlDoc(config, indent_array_in_object=true, quote_keys=false)

# Nuke

This project contains resources for running `aws-nuke`.

**THIS IS AN EXTREMELY DANGEROUS OPERATION.**

**MAKE SURE YOU KNOW EXACTLY WHAT YOU ARE DOING BEFORE YOU RUN THIS.**

## List resources for deletion

```shell
./script.sh


## Delete resources

```shell
./script.sh --no-dry-run
```

## Do it with vengeance

```shell
./nuke-with-vengeance.sh
